-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2021 at 05:14 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ctc_memberkomunitas`
--

-- --------------------------------------------------------

--
-- Table structure for table `c_accsetting`
--

CREATE TABLE `c_accsetting` (
  `cid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `encrypted_account` text NOT NULL,
  `update_by` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `c_accsetting`
--

INSERT INTO `c_accsetting` (`cid`, `name`, `encrypted_account`, `update_by`, `timestamp`) VALUES
(1, 'smile1-gmail', '{\"iv\":\"7109f1c4435f156c4e35d333e7964ccb\",\"content\":\"e38f3b355f58b29ce82627669aed0826173a883cc7c1f7663a0c5962a721ba530fdfb5300e4cc6a88dd7e979ddb88eb8833e947467f71b0cf4794d9193ef3d9ea5\"}', '', '2021-02-16 16:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `c_app_keys`
--

CREATE TABLE `c_app_keys` (
  `cid` int(5) NOT NULL,
  `keyid` varchar(64) NOT NULL,
  `appname` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `c_app_keys`
--

INSERT INTO `c_app_keys` (`cid`, `keyid`, `appname`, `timestamp`) VALUES
(1, '5be929723e47e3bd0df2d41b090bff2a', 'rtusgaming.com', '2021-02-02 05:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `c_backup_list`
--

CREATE TABLE `c_backup_list` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `c_base_menu`
--

CREATE TABLE `c_base_menu` (
  `id` int(11) NOT NULL,
  `title` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `order_no` int(2) NOT NULL,
  `end_point` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `access_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `actions_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `has_child` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_base_menu`
--

INSERT INTO `c_base_menu` (`id`, `title`, `order_no`, `end_point`, `icon`, `access_code`, `actions_code`, `has_child`) VALUES
(1, 'Member', 1, '/master-data', 'fa fa-users', 'c-privilege', 'create-update-member', 1),
(2, 'Finance', 2, '/master-data', 'fa fa-bank', 'c-privilege', '', 1),
(3, 'Donasi', 3, '/master-data', 'fa fa-database', 'c-privilege', '', 1),
(4, 'Setting', 4, '/admin/administrative', 'fa fa-gears', 'c-privilege', 'create-update-user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_menus`
--

CREATE TABLE `c_menus` (
  `id` int(11) NOT NULL,
  `base_id` int(2) NOT NULL,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `end_point` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `actions_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_no` int(1) NOT NULL,
  `enabled` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_menus`
--

INSERT INTO `c_menus` (`id`, `base_id`, `title`, `end_point`, `access_code`, `actions_code`, `order_no`, `enabled`) VALUES
(1, 1, 'File Kupon', '/member/file_kupon', 'c-privilege', 'create-update-kupon', 1, 1),
(2, 1, 'File Kupon Voucher', '/admin/administrative', 'c-privilege', 'create-update-voucher', 2, 1),
(3, 1, 'File Withdraw', '/admin/administrative', 'c-privilege', 'create-update-withdraw', 3, 1),
(4, 1, 'File Total Withdraw', '/admin/administrative', 'c-privilege', 'create-update-totalwithdraw', 4, 1),
(5, 1, 'File Jaringan Mitra', '/admin/administrative', 'c-privilege', 'create-update-mitra', 5, 1),
(6, 2, 'File Bonus Sponsor', '/master-data/fbonus_sponsor', 'c-privilege', 'create-update-sponsor', 6, 1),
(7, 2, 'File Bonus Referensi', '/master-data/fbonus_referensi', 'c-privilege', 'create-update-referensi', 7, 1),
(8, 2, 'File Bonus Matching Kupon', '/master-data/fbonus_matching', 'c-privilege', 'create-update-bonusmatching', 8, 1),
(9, 3, 'File Donasi Masuk', '/admin/administrative', 'c-privilege', 'create-update-donasimasuk', 10, 1),
(10, 3, 'File Donasi Pending', '/admin/administrative', 'c-privilege', 'create-update-donasipending', 11, 1),
(11, 3, 'File Donasi Kurang', '/admin/administrative', 'c-privilege', 'create-update-donasikurang', 12, 1),
(12, 3, 'File Type Donasi', '/master-data/tipe_donasi', 'c-privilege', 'create-update-typedonasi', 9, 1),
(13, 4, 'Syarat & Ketentuan', '/master-data/syarat_ketentuan', 'c-privilege', '', 13, 1),
(14, 4, 'Buat Laporan ke Admin', '/master-data', 'c-privilege', '', 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_users`
--

CREATE TABLE `c_users` (
  `uid` int(11) NOT NULL,
  `uname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `passwd` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `accessibility` text COLLATE utf8_unicode_ci NOT NULL,
  `actions_code` text COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `last_page` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `related_pic` int(5) NOT NULL,
  `template` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'indonesia',
  `profile` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` int(10) NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT 1,
  `created_by` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_by` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_users`
--

INSERT INTO `c_users` (`uid`, `uname`, `passwd`, `email`, `name`, `accessibility`, `actions_code`, `level`, `last_page`, `related_pic`, `template`, `lang`, `profile`, `provider_id`, `enabled`, `created_by`, `created_at`, `update_by`, `updated_at`) VALUES
(136793619, 'admin@codewell.co.id', '87d9bb400c0634691f0e3baaf1e2fd0d', 'admin@codewell.co.id', 'CTC Admin', 'c::setting, c::games', '', 'c-spadmin', '', 0, 'ctc-azia-topbar', 'indonesia', '136793619.png', 1, 1, '', '2021-01-04 14:26:24', '', '2021-01-26 04:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sponsor`
--

CREATE TABLE `tb_sponsor` (
  `id` int(11) NOT NULL,
  `identitas_no` varchar(255) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sponsor`
--

INSERT INTO `tb_sponsor` (`id`, `identitas_no`, `tipe`, `tanggal`, `keterangan`, `jumlah`) VALUES
(1, '12345', 'Hadiah 1', '2021-04-26', 'Bonus sponsor Hadiah 1', 5000000);

-- --------------------------------------------------------

--
-- Table structure for table `test_crud`
--

CREATE TABLE `test_crud` (
  `id` int(11) NOT NULL,
  `identity_no` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `images` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(30) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test_crud`
--

INSERT INTO `test_crud` (`id`, `identity_no`, `first_name`, `last_name`, `gender`, `images`, `created_at`, `created_by`, `timestamp`) VALUES
(1, '2', 'dgd', 'dgfdee', 'laki-laki', '', '2021-03-03 16:52:16', '136793619', '2021-04-28 02:05:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `c_accsetting`
--
ALTER TABLE `c_accsetting`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `name` (`name`),
  ADD KEY `update_by` (`update_by`);

--
-- Indexes for table `c_app_keys`
--
ALTER TABLE `c_app_keys`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `keyid` (`keyid`),
  ADD KEY `app_title` (`appname`);

--
-- Indexes for table `c_backup_list`
--
ALTER TABLE `c_backup_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filename` (`filename`(250)),
  ADD KEY `created_at` (`created_at`);

--
-- Indexes for table `c_base_menu`
--
ALTER TABLE `c_base_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD KEY `access_code` (`access_code`),
  ADD KEY `end_point` (`end_point`),
  ADD KEY `icon` (`icon`),
  ADD KEY `actions_code` (`actions_code`);

--
-- Indexes for table `c_menus`
--
ALTER TABLE `c_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `base_id` (`base_id`,`title`,`end_point`,`access_code`),
  ADD KEY `actions_code` (`actions_code`),
  ADD KEY `enabled` (`enabled`);

--
-- Indexes for table `c_users`
--
ALTER TABLE `c_users`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `uname` (`uname`),
  ADD KEY `passwd` (`passwd`,`email`,`name`,`level`,`enabled`),
  ADD KEY `related_pic` (`related_pic`),
  ADD KEY `template` (`template`),
  ADD KEY `lang` (`lang`),
  ADD KEY `last_page` (`last_page`);

--
-- Indexes for table `tb_sponsor`
--
ALTER TABLE `tb_sponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_crud`
--
ALTER TABLE `test_crud`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `c_accsetting`
--
ALTER TABLE `c_accsetting`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `c_app_keys`
--
ALTER TABLE `c_app_keys`
  MODIFY `cid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `c_backup_list`
--
ALTER TABLE `c_backup_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `c_base_menu`
--
ALTER TABLE `c_base_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `c_menus`
--
ALTER TABLE `c_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tb_sponsor`
--
ALTER TABLE `tb_sponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `test_crud`
--
ALTER TABLE `test_crud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
