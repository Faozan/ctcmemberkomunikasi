<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Fbonus_sponsor_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tableSponsor = "tb_sponsor";
		$this->load->helper('ctc');
	}
	function _load_dt($posted)
	{
		$orders_cols = ["id","identitas_no", "tipe_bonus", "jumlah", "tanggal", "keterangan"];
		$output = build_filter_table($posted, $orders_cols);
		$sWhere = $output->where;
		if (isset($output->search) && $output->search != "") {
			$sWhere = "WHERE identitas_no LIKE '%" . $output->search . "%' OR tipe_bonus LIKE '%" . $output->search . "%'";
		}
		$sLimit = $output->limit;
		$sGroup = "";
		$sOrder = $output->order;
		$limit = 0;
		$offset = 25;
		$data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS " . implode(",", $orders_cols) . " FROM tb_sponsor $sWhere $sGroup $sOrder $sLimit")->result_object();
		$found = $this->db->query("SELECT FOUND_ROWS() as total")->result_object();
		$map_data = array_map(function ($dt) {
			$id = $dt->id;
			return [
				$dt->identitas_no,
				$dt->tipe_bonus,
				$dt->jumlah,
				$dt->tanggal,
				$dt->keterangan,
				'<a href="#" class="link-edit-crud" data-id="' . $dt->id . '"><i class="fa fa-edit"></i></a>  &nbsp;
						<a href="#" class="link-delete-crud" data-id="' . $dt->id . '"><i class="fa fa-trash text-danger"></i></a>
						'
			];
		}, $data);
		$output->recordsTotal = (sizeof($found) == 0) ? 0 : (int) $found[0]->total;
		$output->recordsFiltered = $output->recordsTotal;
		$output->data = $map_data;
		return (array) $output;
	}
	function _search($where)
	{
		$this->db->select('id as _id,identity_no,first_name,last_name,gender');
		$this->db->from($this->tableSponsor);
		$this->db->where($where);
		return $this->db->get()->result();
	}
	function _search_select2($key = "")
	{
		$this->db->select('id,first_name as text');
		$this->db->from($this->tableDokter);
		if ($key != "") {
			$this->db->like('first_name', $key);
		}
		$this->db->limit(30);
		return $this->db->get()->result_array();
	}
	function _save($data, $where, $key)
	{
		if (empty($where)) {
			// check before insert
			$this->db->select($key)->from($this->tableSponsor)->where($key, $data[$key]);
			$check = $this->db->get()->result();
			if (!empty($check)) return 'exist';
			$this->db->insert($this->tableSponsor, $data);
			return $this->db->affected_rows();
		} else {
			$this->db->select('id')->from($this->tableSponsor);
			$this->db->where($key, $data[$key]);
			$this->db->where("id!=", $where['id']);
			$check = $this->db->get()->result();
			if (!empty($check)) return 'exist';
			$this->db->update($this->tableSponsor, $data, $where);
			return $this->db->affected_rows();
		}
	}
	function _delete($where)
	{
		$this->db->delete($this->tableSponsor, $where);
		return $this->db->affected_rows();
	}
}

/* End of file Pasien_model.php */
