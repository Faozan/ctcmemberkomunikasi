<div class="row">
	<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card bd-0 with-border">
			<div class="card-header" style="background: gold;">
				<div class="card-title widget-content-left"><span class="fa fa-align-justify">&nbsp; &nbsp;</span>LIST JARINGAN MITRA
					<div class="pull-right">
					<button type="button" class="btn btn-primary add-crud btn-xs"><i class="fa fa-plus"></i> Add</button>
					<button type="button" class="btn btn-success export btn-xs"><i class="fa fa-download"></i>Export</button>
					</div>
				</div>
			</div>
			<div class="card-body">
				<table class="table table-bordered dataTable table-striped" style="margin-left:auto;margin-right:auto" id="dataCrud" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0">
					<thead>
						<tr role="row">
							<th class="search" data-name="identitas_no">Identitas No</th>
							<th class="search" data-name="nama">Nama</th>
							<th class="search" data-name="username">Username</th>
                            <th class="search" data-name="tanggal_daftar">Tanggal Daftar</th>
							<th class="search" data-name="keterangan">Keterangan</th>

						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</section>
</div>

<div id="modal-manage-crud" class="modal">
  <div class="modal-dialog" role="document">
  	<form method="POST" name="form-manage-crud">
	    <div class="modal-content modal-content-demo">
	      <div class="modal-header">
	        <h6 class="modal-title">Add/Update Data</h6>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group">
								<label class="form-label">Identitas No</label>
								<input type="text" class="form-control input-sm" name="identitas_no" id="identitas_no" placeholder="Identitas No">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group">
								<label class="form-label">Nama</label>
								<input type="text" class="form-control input-sm" name="nama" id="nama" placeholder="Nama">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group">
							<label for="username">Username</label>
                            <input type="text" class="form-control" name="username" name="username" id="username" placeholder="Username">
							</div>
						</div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group">
							<label for="tanggal">Tanggal Daftar</label>
                            <input type="datepicker" class="form-control" name="tanggal" name="tanggal" id="tanggal" placeholder="Tanggal">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group">
							<label for="keterangan">Keterangan</label>
                            <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="keterangan">
							</div>
						</div>
						
					</div>
					<input type="text" class="hide" name="_id" value="">
				</div>
	      <div class="modal-footer">
					<button type="submit" id="save-crud" class="btn btn-indigo"><i class="fa fa-save"></i> <?=lang('label_save');?></button>
	        <button type="button" class="btn btn-outline-light" data-dismiss="modal"><?=lang('label_close');?></button>
	      </div>
	    </div>
	</form>
  </div><!-- modal-dialog -->
</div><!-- modal -->
<div id="modal-manage-import" class="modal">
  <div class="modal-dialog modal-lg" role="document">
  	<form method="POST" name="form-manage-import">
	    <div class="modal-content modal-content-demo">
	      <div class="modal-header">
	        <h6 class="modal-title"><?=lang('label_title_import');?></h6>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
					<div class="card card-body bg-indigo tx-white bd-0">
						<h5 class="card-title tx-white tx-medium mg-b-5">CATATAN:</h5>
						<p class="card-text">
						<ul class="m-0 pl-3">
							<li>Hanya file excel yang dapat diproses untuk import</li>
							<li>Format urutan kolom harus disesuaikan dengan sample yang ada. Jika belum ada silahkan download <a href="<?=base_url('files/docs/YM-FormatImportDatacrud.xlsx');?>" class="tx-warning" target="_blank">disini</a></li>
						</ul>
						</p>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<label class="form-label">Format: </label>
							<img src="<?=base_url('assets/img/image-sample-import-crud.png');?>" class="zoomImage" style="max-width: 100%;border: 1px solid #ff0000">
						</div>
					</div>
					<div class="row"><hr></div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="card border-1">
								
								<div class="rows">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
											<label class="form-label"><?=lang('label_choose_file');?></label>
										</div>
										<div class="btn btn-xs p-0 btn-import">
											<button type="button" class="btn btn-warning choose_file__ btn-xs"><i class="fa fa-folder-open-o"></i> <span><?=lang('label_browse');?></span></button>
											<input type="file" name="file" class="hide" id="file" accept=".xls,.xlsx">
											<span class="filename"></span>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
										<div class="form-group">
											<label class="form-label">Index Row Data</label>
											<input type="number" class="form-control input-sm" name="start_row" value="7">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 row-error">
							<h5 class="title m-0 pb-2 tx-bold tx-danger"></h5>
							<div class="content" style="height: 170px; overflow-y: auto;"></div>
						</div>
					</div>
					
				</div>
	      <div class="modal-footer inline-block">
					<button type="submit" id="submit-import" class="btn btn-indigo"><i class="fa fa-save"></i> Submit</button>
					<button type="button" class="btn btn-outline-light pull-right" data-dismiss="modal"><?=lang('label_close');?></button>
	      </div>
	    </div>
	</form>
  </div><!-- modal-dialog -->
</div><!-- modal -->

