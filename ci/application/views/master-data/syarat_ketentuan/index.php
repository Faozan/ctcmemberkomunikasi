<div class="row">
	<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card bd-0">
            <div class="card-header" style="background: #C1FCDA;">
				<div class="card-title widget-content-left"><span class="fa fa-align-justify">&nbsp; &nbsp;</span>ATURAN LAYANAN
				  	<?=lang('label_list_patient');?> 
					<?php
						if(isset($import_id)){
							echo "<input type='hidden' id='import_id' value='".$import_id."'><br>
							<div class='tx-primary txt-normal mt-2'>[Data yang ditampilkan adalah yang baru saja anda import] 
							<a class='text-normal tx-danger' href='".base_url('crud')."' title='Click disini untuk kembali ke keseluruhan data crud'><sup><i class=''></i> clear filter</sup></a>
							</div>
							
							";
						}
					?>
				</div>

			</div>
			<div class="card-body">
				<!-- CONTENT SECTION -->
                <section class="ftco-section">
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row justify-content-center pb-2">
                            <div class="col-md-12 heading-section ftco-animate">
                                <p>
                                   <b> Syarat Dan Peraturan Member Komunitas </b>
                                </p>
                            </div>
                            <div class="col-md-12 ftco-animate">
                                <ul class="list-group">

                                <li class="list-group-item">Kami bukan Bank atau Lembaga Keuangan</li>
                                    <li class="list-group-item">Kami juga bukan Bisnis Multi Level Marketing
                                    <li class="list-group-item">Kami adalah Komunitas Emas, saling memberi gift emas, edukasi emas & peluang emas.</li>

                                    <li class="list-group-item">"Mari  dengan bersama sama membeli & menyimpan emas, sehingga dari langkah kecil bila dilakukan bersama sama bisa membuat harga emas naik, sehingga kita dapat sama2  untung dari jual dan beli emas"
                                    </li>
                                    <li class="list-group-item">Kami mencintai emas dengan memberikan gift/hadiah emas pada Saudara Kami</li>
                                    <li class="list-group-item">Kami bersama-sama Membangun Masa Depan penuh ke Emasan</li>

                                    <li class="list-group-item">Member yang bergabung disini adalah murni karena kesadaran sendiri, tanpa adanya paksaan, tekanan, iming-iming atau janji-janji yang diberikan oleh pihak lain, sehingga semua resiko menjadi tanggung jawab sendiri.</li>

                                    <li class="list-group-item">Uang yang Anda transfer adalah bersifat dan memiliki nilai intrinsik emas berupa gift/hadiah,  bukan donasi  & tidak berharap untuk dikembalikan dalam bentuk apapun.</li>

                                    <li class="list-group-item">GG tidak memberikan jaminan atau janji dan hadiah kepada setiap Member.</li>

                                    <li class="list-group-item">Peringatan  : Dilarang keras menggunakan uang yang bersumber dari Pinjaman, Hutang, Kartu kredit , Gadai dan sebagainya untuk melakukan gift emas.</li>

                                    <li class="list-group-item">Tidak ada pengepulan atau pengumpulan dana di Member Komunitas</li>
                                    
                                </ul>
                            </div>
                            <div>
                                <br><p><b>Anda telah menyetujui Syarat dan Ketentuan yang berlaku</b>            
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END CONTENT SECTION -->
            </div>
		</div>
	</section>
</div>

