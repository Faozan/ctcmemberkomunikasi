<body class="az-body">
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="card">
                    <div class="card-header bg-secondary bd-b-0-f pd-b-0">
                        <nav class="nav nav-tabs">
                            <a class="nav-link active" data-toggle="tab" href="#tabCont1"><b>LEVEL 1 SILVER</b></a>
                            <a class="nav-link" data-toggle="tab" href="#tabCont2"><b>LEVEL 2 GOLD</b></a>
                            <a class="nav-link" data-toggle="tab" href="#tabCont3"><b>LEVEL 3 PLATINUM</b></a>
                        </nav>
                    </div><!-- card-header -->
                    <div class="card-body bd bd-t-0 tab-content">
                        <div id="tabCont1" class="tab-pane active show">
                            <div class="az-content-label mg-b-5"></div>
                            <p class="mg-b-20"></p>
                            <div class="container">
                                <div class="row">

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: silver;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 400.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: silver;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 400.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-warning btn-icon rounded-circle"><i class="fa fa-minus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: silver;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">UPGRADE <br>2 KUPON</h3>
                                                <b>
                                                    <p class="card-text">@ RP 100.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-money">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: silver;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 400.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                </div><!-- row -->
                            </div><!-- tab-pane -->
                        </div><!-- tabcont1 -->
                        <div id="tabCont2" class="tab-pane">
                            <div class="az-content-label mg-b-5"></div>
                            <p class="mg-b-20"></p>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-warning btn-icon rounded-circle"><i class="fa fa-minus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: gold;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">UPGRADE <br>5 KUPON</h3>
                                                <b>
                                                    <p class="card-text">@ RP 100.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-money">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->
                                </div><!-- row -->
                            </div>
                        </div><!-- tabcont2 -->
                        <div id="tabCont3" class="tab-pane">
                            <div class="az-content-label mg-b-5"></div>
                            <p class="mg-b-20"></p>
                            <div class="container">
                                <div class="row">

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: lightsteelblue;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 3.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-warning btn-icon rounded-circle"><i class="fa fa-minus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: lightsteelblue;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">UPGRADE <br>10 KUPON</h3>
                                                <b>
                                                    <p class="card-text"> @ RP 100.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-money">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-success btn-icon rounded-circle"><i class="fa fa-plus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: lightsteelblue;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">TERIMA DONASI</h3>
                                                <b>
                                                    <p class="card-text"> RP 3.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-warning btn-icon rounded-circle"><i class="fa fa-minus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: lightsteelblue;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">KIRIM KE LEVEL 2</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->

                                    <div class="icon-list" style="margin: auto;">
                                        <button class="btn btn-warning btn-icon rounded-circle"><i class="fa fa-minus"></i></button>
                                    </div>

                                    <div class="col-md mg-t-20 mg-md-t-0">
                                        <div class="card rounded-left rounded-right" style="width:150px; background-color: lightsteelblue;">
                                            <div class="card-body text-center">
                                                <h3 class="card-title">KIRIM KE LEVEL 2</h3>
                                                <b>
                                                    <p class="card-text"> RP 1.000.000,00</p>
                                                </b>
                                            </div>
                                            <div class="card-footer text-center">
                                                <h4>
                                                    <p class="card-text text-center fa fa-database">
                                                </h4>
                                            </div>
                                        </div><!-- card -->
                                    </div><!-- col -->
                                </div>
                            </div><!-- card-body -->
                        </div><!-- tabcont3 -->
                    </div><!-- card-body -->
                </div><!-- card -->
            </div><!-- content-body -->
        </div><!-- container -->
    </div><!-- az-content -->
</body>

</html>