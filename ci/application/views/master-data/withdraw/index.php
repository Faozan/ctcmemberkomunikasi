<div class="az-signin-wrapper" style="background: white">
	<div class="az-card-withdraw col-lg-6">
		<div class="form">
			<div class="alert alert-warning" role="alert">
				<div class="form-group row">
					<label for="staticSaldo" class="col-sm-3 col-form-label"><b>Saldo Anda saat ini</b></label>
					<div class="col-sm-4">
						<input type="text" readonly class="form-control-plaintext" id="staticSaldo" value="Rp 10.000.000,00">
					</div>
				</div>
				<div class="form-group row">
					<label for="staticBatas" class="col-sm-3 col-form-label"><b>Batas penarikan</b></label>
					<div class="col-sm-4">
						<input type="text" readonly class="form-control-plaintext" id="staticBatas" value="Rp 10.000.000,00">
					</div>
				</div>
				<div class="form-group row">
					<label for="staticJumlah" class="col-sm-3 col-form-label"><b>Jumlah minimum penarikan </b></label>
					<div class="col-sm-4">
						<input type="text" readonly class="form-control-plaintext" id="staticJumlah" value="Rp 100.000,00">
					</div>
				</div>
			</div>
		</div>

		<section class="about">
			<div class="card">
				<div class="card-body">
					<form class="form input">
						<div class="form-group row">
							<label for="inputPenarikan" class="col-sm-3 col-form-label">Jumlah Penarikan</label>
							<div class="input-group-prepend c" style="width: 250px;">
								<span class="input-group-text">RP</span>
								<input type="text" class="form-control" id="inputPenarikan" placeholder="00">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputUsername" class="col-sm-3 col-form-label">Username</label>
							<div class="input-group-prepend c" style="width: 250px;">
								<input type="email" class="form-control" id="inputUsername" placeholder="-- Masukkan username anda --">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-3 col-form-label">Password</label>
							<div class="input-group-prepend c" style="width: 250px;">
								<input type="password" class="form-control" id="inputPassword3" placeholder="-- Masukkan password anda --">
							</div>
						</div>

						<div class="row offset-5">
							<button type="submit" class="btn btn-primary rounded-left rounded-right" href="<?= base_url('admin/auth/signin'); ?>">Konfirmasi</button>
						</div>


					</form>
				</div>
			</div>
		</section>
	</div>
</div>