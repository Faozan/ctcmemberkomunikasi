<!DOCTYPE html>
<html lang="en">
<?php
$src_assets_template = "assets/azia-assets";
$src_view_template = "template/ctc-leftm";
?>
<!-- page-signin.html -->

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?= isset($meta_description) ? $meta_description : ''; ?>">
  <meta name="author" content="<?= isset($meta_author) ? $meta_author : 'PT. Codewell Tekindo Cemerlang'; ?>">
  <title><?= isset($page_title) ? $page_title : "SignIn"; ?></title>
  <!-- vendor css -->
  <link href="<?= base_url($src_assets_template . '/lib/font-awesome-4.7.0/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url($src_assets_template . '/lib/lightslider/css/lightslider.min.css'); ?>" rel="stylesheet">
  <!-- azia CSS -->
  <link rel="stylesheet" href="<?= base_url($src_assets_template . '/css/azia.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/ctc.css'); ?>">
  <link href="<?= base_url($src_assets_template . '/lib/disk/slidercaptcha.css'); ?>" rel="stylesheet" />
  <link href="<?= base_url($src_assets_template . '/lib/disk/slidercaptcha.min.css'); ?>" rel="stylesheet" />
</head>

<body class="az-body body-login">
  <div class="az-signin-wrapper" style="background-image: <?= base_url('assets/img/bg-lock.png'); ?>">
    <div class="az-card-signin">
      <div class="az-signin-header">
        <h2 class="signin-title text-primary">
          <img src="<?= base_url(conf('company_logo')); ?>" />
          <?= conf('company_name'); ?>

        </h2>
        <h4 class="text-disabled"><?= lang('label_title_to_signin'); ?></h4>
        <?php $redirect = (isset($redirect)) ? "?redirect=$redirect" : "";  ?>
        <form action="<?= base_url('admin/auth/signin' . $redirect); ?>" method="post">

          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" placeholder="<?= lang('label_input_username'); ?>" value="" autocomplete="off" required autofocus="">
          </div><!-- form-username -->

          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="<?= lang('label_input_password'); ?>" autocomplete="off" required>
          </div><!-- form-password -->

          <div class="row">
            <div class="card-body">
              <div class="card" style="width: 20rem;" id="captcha"><?php echo $this->session->flashdata("message"); ?></div>
            </div>
          </div>

          <script>
            var captcha = sliderCaptcha({
              id: 'captcha',
              repeatIcon: 'fa fa-redo',
              onSuccess: function() {
                var handler = setTimeout(function() {
                  window.clearTimeout(handler);
                  captcha.reset();
                }, 500);
              }
            });
          </script>

          <div class="form-group">
            <div class="alert-error-login"><?php echo $this->session->flashdata("message"); ?></div>
          </div>
          <button class="btn btn-primary btn-block"><i class="fa fa-sign-in"></i> Masuk</button>


          <div class="az-signin-footer">
            <p>&nbsp;</p>
            <p>
              <a class="text-warning" href="#" onclick="javscript:alert('Anda dapat request reset password ke YM Pusat')"><?= lang('label_forget_password'); ?></a>
              <a class="pull-right btn btn-xs btn-info text-white" href="<?= base_url('register'); ?>"> Daftar</a>
            </p>
          </div><!-- az-signin-footer -->

        </form>
      </div><!-- az-signin-header -->
    </div><!-- az-card-signin -->
  </div><!-- az-signin-wrapper -->

  <script src="<?= base_url($src_assets_template . '/lib/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url($src_assets_template . '/lib/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?= base_url($src_assets_template . '/js/azia.js'); ?>"></script>
  <script src="<?= base_url($src_assets_template . '/lib/disk/longbow.slidercaptcha.js'); ?>"></script>
  <script src="<?= base_url($src_assets_template . '/lib/disk/longbow.slidercaptcha.min.js'); ?>"></script>
  <script src="<?= base_url($src_assets_template . '/lib/disk/main.js'); ?>"></script>
  <script>
    $(document)
      .on('keypress', "input", function(e) {
        if (e.keyCode != 13) {
          $(".alert-error-login").html('');
        }
      })
  </script>
  <script>
    var captcha = sliderCaptcha({
      id: 'captcha',
      repeatIcon: 'fa fa-redo',
      onSuccess: function() {
        var handler = setTimeout(function() {
          window.clearTimeout(handler);
          captcha.reset();
        }, 500);
      }
    });
  </script>
</body>
<!-- page-signin.html -->

</html>