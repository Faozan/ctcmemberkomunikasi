<!DOCTYPE html>
<html lang="en">
<?php
$src_assets_template = "assets/azia-assets";
$src_view_template = "templates/ctc-leftm";
$time = (int) rand();
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= isset($meta_description) ? $meta_description : ''; ?>">
    <meta name="author" content="<?= isset($meta_author) ? $meta_author : 'PT. Codewell Tekindo Cemerlang'; ?>">
    <title><?= isset($page_title) ? $page_title : "Sign up"; ?></title>
    <!-- vendor css -->
    <link href="<?= base_url($src_assets_template . '/lib/font-awesome-4.7.0/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url($src_assets_template . '/lib/lightslider/css/lightslider.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url($src_assets_template . '/lib/select2/css/select2.min.css'); ?>" rel="stylesheet">
    <!-- azia CSS -->
    <link rel="stylesheet" href="<?= base_url($src_assets_template . '/css/azia.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/ctc.css'); ?>">
</head>

<body class="az-body mt-8-mb-8">
    <div class="az-signin-wrapper" style="background: #ccc">
        <div class="az-card-signin col-lg-4">
            <div class="az-signin-header">
                <h2 class="signin-title text-primary"><img src="<?= base_url(conf('company_logo')); ?>" /><?= conf('company_name'); ?></h2>
                <h4 class="text-disabled">Silakan lengkapi data anda untuk registrasi sebagai calon member</h4>
                <?php $redirect = (isset($redirect)) ? "?redirect=$redirect" : "";  ?>
                <form class="form-horizontal" method="POST" action="<?php echo site_url('auth/signup'); ?>">
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text" class="form-control" id="inputNama" placeholder="-- Isi nama lengkap anda --" autocomplete="off" required>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="datepicker">Tanggal Lahir</label>
                            <input type="datepicker" class="form-control" id="tgl_lahir" placeholder="-- Isi tanggal lahir anda --" autocomplete="off" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="nomorHP">Nomor HP</label>
                            <input type="text" class="form-control" id="inputnomor" placeholder="-- Isi nomor HP anda --" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat </label>
                        <input type="text" class="form-control" id="inputAlamat" placeholder="-- Isi alamat tempat tinggal anda --" autocomplete="off" required>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <select id="inputProvinsi" class="form-control">
                                <option selected>Pilih Provinsi</option>
                                <option>...</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <select id="inputKota" class="form-control">
                                <option selected>Pilih Kabupaten/Kota</option>
                                <option>...</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputUsername">Username</label>
                            <input type="email" class="form-control" id="inputUsername" placeholder="-- Isi username anda --" autocomplete="off" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail">Email</label>
                            <input type="email" class="form-control" id="inputEmail" placeholder="-- Isi email anda --" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword">Password</label>
                            <input type="password" class="form-control" id="inputPassword" placeholder="-- Isi password anda --" autocomplete="off" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputUlangipassword">Ulangi Password</label>
                            <input type="password" class="form-control" id="inputUlangipassword" placeholder="-- Ulangi password anda --" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="keamananpassword" class="form-control" id="inputKeamananPassword" placeholder="Keamanan Password" autocomplete="off" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="password" class="form-control" id="inputKonfirmasi_keamanan_password" placeholder="Konfirmasi Keamanan Password" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <select id="inputBank" class="form-control">
                                <option selected>Pilih Bank</option>
                                <option>...</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <select id="inputCabang" class="form-control">
                                <option selected>Pilih Cabang</option>
                                <option>...</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="No_Rekening" class="form-control" id="inputNo_Rekening" placeholder="No.Rekening" autocomplete="off" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="Nama_di_Rekening" class="form-control" id="inputNama_di_Rekening" placeholder="Nama di Rekening" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-xs btn-primary text-white col-md-4">Register</button>
                    </div>
                </form>
            </div><!-- az-signup-header -->
            <div class="az-signin-footer">
                <p>&nbsp;</p>
                <p>Ada akun? click <a class="btn btn-xs btn-az-primary text-white" href="<?= base_url('admin/auth/signin'); ?>">Masuk</a>
                    <input type="hidden" id="base_url" value="<?= base_url(); ?>">
                </p>
            </div>
        </div><!-- az-card-signup -->
    </div><!-- az-signup-wrapper -->


    <script src="<?= base_url($src_assets_template . '/lib/jquery/jquery.min.js'); ?>"></script>
    <script src="<?= base_url($src_assets_template . '/lib/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url($src_assets_template . '/lib/jquery-ui/ui/widgets/datepicker.js'); ?>"></script>
    <script src="<?= base_url($src_assets_template . '/lib/select2/js/select2.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/jQuery.print.min.js?pid=' . $time); ?>"></script>
    <script src="<?= base_url('assets/pages/bootbox-custom.min.js?pid=' . $time); ?>"></script>
    <script src="<?= base_url('assets/pages/lodash.min.js'); ?>"></script>
    <script src="<?= base_url($src_assets_template . '/js/azia.js'); ?>"></script>
    <script src="<?= base_url('assets/pages/ctc.js?pid=' . $time); ?>"></script>

    <script>
        $(document)
            .on('keypress', "input", function(e) {
                if (e.keyCode != 13) {
                    $(".alert-error-login").html('');
                }
            })
            .on("click", "#submit_register", function(e) {
                e.preventDefault()
                var form = $(this).closest('form').serialize();
                http_request('admin/auth/signout', 'POST', form)
                    .done(function(res) {
                        if (res.action = "call-print") {
                            var modalRegSuccessID = "#modal-reg-success";
                            var _body = $(modalRegSuccessID).find('.modal-body');
                            $.each(res.data, function(key, val) {
                                $(_body).find("." + key).text(val);
                            })
                            $(modalRegSuccessID).modal({
                                    effect: 'effect-slide-in-right',
                                    backdrop: 'static',
                                    keyboard: false,
                                    show: true
                                })
                                .on('hidden.bs.modal', function() {
                                    setTimeout(function() {
                                        location.href = base_url('admin/auth');
                                    }, 2000)
                                })
                        } else {
                            Msg.success(res.message);
                            setTimeout(function() {
                                location.href = base_url('auth');
                            }, 2000)
                        }
                    })
            })
            .on("click", ".btn-print", function() {
                var printarea = $(this).data("printarea");
                $(printarea).print({
                    globalStyles: true,
                    mediaPrint: true,
                    stylesheet: null,
                    noPrintSelector: ".no-print",
                    iframe: true,
                    append: null,
                    prepend: null,
                    manuallyCopyFormValues: true,
                    deferred: $.Deferred(),
                    timeout: 750,
                    title: $(this).data("title-print"),
                    doctype: '<!doctype html>'
                });
            })
        $("#search_provider").select2({
            minimumInputLength: 0,
            allowClear: false,
            multiple: false,
            placeholder: 'click untuk mencari',
            ajax: {
                url: base_url('auth/search-provider-select2-'),
                headers: {
                    'x-user-agent': 'ctc-webapi'
                },
                data: function(params) {
                    return {
                        search: params.term
                    }
                },
                processResults: function(data) {
                    return {
                        results: data
                    };
                }
            },
        })

        $('[id="tgl_lahir"]').datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            reverseYearRange: true,
            yearRange: 'c-80:c+10',
        });
    </script>
</body>
<!-- page-signout.html -->

</html>