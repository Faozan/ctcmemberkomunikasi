$modalManageObat = "#modal-manage-obat";
$modalManageSatuanbeli = "#modal-manage-satuan-beli";
$modalManageSatuanobat = "#modal-manage-satuan-obat";
$modalManageKategori = "#modal-manage-kategori";
$modalManageImport = "#modal-manage-import";
$modalManageBarcode = "#modal-manage-barcode";
$tableData = $("#dataObat").DataTable({

    serverSide: true,
    ordering: true,
    pageLength: "50",
    ajax: {
        url: base_url('farmasi/obat/load-dt'),
        type: 'POST',
        headers: {
            'x-user-agent': 'ctc-webapi',
        },
        data: function(d) {
					if ($("#import_id").length > 0 && $("#import_id").val() != "") d.import_id = $("#import_id").val();
        }

    },
	
    language: DataTableLanguage(),
    responsive: true,
    scrollY: '50vh',
    scrollCollapse: true,
    scrollX: false,
    order: [[1,'desc']],
    columnDefs: [
		{ targets: [0], width: '35px',className: 'text-center' },
    ],
    rowCallback: function(row, data, iDisplayIndex){	
		var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
    },
})
$(document)
	.ready(function () {
		getActiveLang('farmasi/obat');
	})

	.on("click", ".add-obat", function () {
		$modal_id=$modalManageObat;
		$modal_body=$($modal_id).find('.modal-body');
		
		$modal_body.find("input:text").val("");
		$modal_body.find("select").val('').trigger('change');

		$modal_body.find("#autocode").prop('disabled', false);

		$modal_body.find(".show-on-update").addClass('hide');
		$($modal_id).modal({
			effect: 'effect-slide-in-right',
			backdrop: 'static',
			keyboard: false,
			show: true
		})
	})
    
	.on("click",".link-edit-obat",function(){
		http_request('farmasi/obat/search__','GET',{id: $(this).data('id')})
		.done(function(result){
			var data=result.data;
			$modal_id=$modalManageObat;
			$modal_body = $($modal_id).find('.modal-body');
			
			$('#satuanbeli').html('<option value="'+data.satuanbeli+'">'+data.namaSatuanbeli+'</option>').trigger('change');
			$('#satuanobat').html('<option value="'+data.satuanobat+'">'+data.namaSatuanobat+'</option>').trigger('change');
			$('#kategori').html('<option value="'+data.kategori+'">'+data.namaKategoriobat+'</option>').trigger('change');
			$('#supplier').html('<option value="'+data.supplier+'">'+data.namaSupplier+'</option>').trigger('change');
			
			$.each(data, function (key, val) {
					$modal_body.find("[name='"+ key +"']").val(val);
			})
			
			$("#autocode").prop('disabled', true);

			$($modal_id).modal({
					effect: 'effect-slide-in-right',
					backdrop: 'static',
					keyboard: false,
					show: true
			})
		})
	})
	.on("submit","form[name='form-manage-obat']",function(e){
		e.preventDefault();
		$("#save-obat").attr('disabled', 'disabled');
		$form=$(this).closest('form');
		var data = $form.serialize();
		http_request('farmasi/obat/save__','POST',data)
		.done(function(res){
			$($modalManageObat).modal('hide');
			$tableData.ajax.reload();
			Msg.success(res.message);
			$("#save-obat").removeAttr('disabled');
		})
			.fail(function () {
				$("#save-obat").removeAttr('disabled');
			})
			.always(function () {
			$("#save-obat").removeAttr('disabled');
		})
		
	})
	.on("click",".link-delete-obat",function(){
		hideMsg();
		var id=$(this).data('id');
		$that=$(this);
		bootbox.confirm({
			title: $lang.bootbox_title_confirmation,
			message: $lang.bootbox_message_confirm_remove,
			size: 'small',
			buttons: {
				confirm: {
					label: $lang.bootbox_btn_confirm,
					className: 'btn-success'
				},
				cancel: {
					label: $lang.bootbox_btn_no,
					className: 'btn-danger'
				}
			},
			callback: function (result) {
						if(result){
							http_request('farmasi/obat/delete__/'+id,'DELETE',{})
							.done(function(res){
									Msg.success(res.message);
									$tableData.ajax.reload(null,false);
							})
						}
			}
		});
		})


	.on("click",".link-barcode-obat",function(){
		http_request('farmasi/obat/barcode','GET',{id: $(this).data('id')})
		.done(function(result){
			var data=result.data;
			$modal_id=$modalManageBarcode;
			$modal_body = $($modal_id).find('.modal-body');
			
			var code = data.kode
				$("#barcode").JsBarcode(code, {
				width: 1.6,
				displayValue: false,
				height: 50,
				marginTop: 1,
				marginBottom: 1,
			});

			$('#satuanbeli').html('<option value="'+data.satuanbeli+'">'+data.namaSatuanbeli+'</option>').trigger('change');
			$('b#harga').html('Rp'+data.hargaJual+',00');
			$('b#namaobat').html(data.nama);
			$('b#kode').html(data.kode);
			

			$('b#namaobat').each(function (f) {

				var newstr = $(this).text().substring(0,16);
				$(this).text(newstr);
		  
			});


			$($modal_id).modal({
					effect: 'effect-slide-in-right',
					backdrop: 'static',
					keyboard: false,
					show: true
			})
		})
	})


	.on("click", ".add-satuan-beli", function () {
			$($modalManageObat).modal('hide');
			$modal_id=$modalManageSatuanbeli;
			$modal_body=$($modal_id).find('.modal-body');
			
			$modal_body.find("input:text").val("");
			$modal_body.find(".show-on-update").addClass('hide');
			$($modal_id).modal({
				effect: 'effect-slide-in-right',
				backdrop: 'static',
				keyboard: false,
				show: true
			})
		})

	.on("submit","form[name='form-manage-satuan-beli']",function(t){
		t.preventDefault();
		$("#save-satuan-bali").attr('disabled', 'disabled');
		$form=$(this).closest('form');
		var data = $form.serialize();
		http_request('farmasi/obat/save_satuan_beli','POST',data)
		.done(function(res){
			$($modalManageSatuanbeli).modal('hide');
			$tableData.ajax.reload();
			Msg.success(res.message);
			$("#save-satuan-beli").removeAttr('disabled');
		})
			.fail(function () {
				$("#save-satuan-beli").removeAttr('disabled');
			})
			.always(function () {
			$("#save-satuan-beli").removeAttr('disabled');
			$($modalManageObat).modal('show');
		})
	})
		
	
	.on("click", ".add-satuan-obat", function () {
			$($modalManageObat).modal('hide');
			$modal_id=$modalManageSatuanobat;
			$modal_body=$($modal_id).find('.modal-body');
			
			$modal_body.find("input:text").val("");
			$modal_body.find(".show-on-update").addClass('hide');
			$($modal_id).modal({
				effect: 'effect-slide-in-right',
				backdrop: 'static',
				keyboard: false,
				show: true
			})
		})

	.on("submit","form[name='form-manage-satuan-obat']",function(t){
		t.preventDefault();
		$("#save-satuan-obat").attr('disabled', 'disabled');
		$form=$(this).closest('form');
		var data = $form.serialize();
		http_request('farmasi/obat/save_satuan_obat','POST',data)
		.done(function(res){
			$($modalManageSatuanobat).modal('hide');
			$tableData.ajax.reload();
			Msg.success(res.message);
			$("#save-satuan-obat").removeAttr('disabled');
		})
			.fail(function () {
				$("#save-satuan-obat").removeAttr('disabled');
			})
			.always(function () {
			$("#save-satuan-obat").removeAttr('disabled');
			$($modalManageObat).modal('show');
		})
	})
		
	
	
	.on("click", ".add-kategori", function () {
		$($modalManageObat).modal('hide');
		$modal_id=$modalManageKategori;
		$modal_body=$($modal_id).find('.modal-body');
		
		$modal_body.find("input:text").val("");
		$modal_body.find(".show-on-update").addClass('hide');
		$($modal_id).modal({
			effect: 'effect-slide-in-right',
			backdrop: 'static',
			keyboard: false,
			show: true
		})
	})

	.on("submit","form[name='form-manage-kategori']",function(t){
		t.preventDefault();
		$("#save-kategori").attr('disabled', 'disabled');
		$form=$(this).closest('form');
		var data = $form.serialize();
		http_request('farmasi/obat/save_kategori','POST',data)
		.done(function(res){
			$($modalManageKategori).modal('hide');
			$tableData.ajax.reload();
			Msg.success(res.message);
			$("#save-kategori").removeAttr('disabled');
		})
			.fail(function () {
				$("#save-kategori").removeAttr('disabled');
			})
			.always(function () {
			$("#save-kategori").removeAttr('disabled');
			$($modalManageObat).modal('show');
		})
	})


	
		// control import
		.on("click", ".import__", function () {
			var modal_id=$modalManageImport;
			var modal_body=$(modal_id).find('.modal-body');
			// $modal_body.find('input[name="user_id"]').val('');
			
			modal_body.find("input:text").val("");
			// modal_body.find("textarea[name='alamat']").val("");
			$(".row-error .title").html('');
			$(".row-error .content").html('');
			// $modal_body.find("[name*='password']").attr('required');
			modal_body.find(".show-on-update").addClass('hide');
			$(modal_id).modal({
				effect: 'effect-slide-in-right',
				backdrop: 'static',
				keyboard: false,
				show: true
			})
		})
		.on("click", ".choose_file__", function () {
			$(this).closest('div').find('input:file').trigger('click');
		})
		.on("change", "input:file[name='file']", function () {
			$that = $(this);
			var btn_control = $(this).closest('div').find('.btn');
			btn_control.removeClass('btn-warning').addClass('btn-primary');
			// btn_control.find('i.fa').addClass('fa-spin');
			// btn_control.find('span').html('Uploading.. <i class="fa fa-spinner fa-spin"></i>');
			let file = document.getElementById("file").files[0];
			if (file.name == "") return false;
			$that.closest('div').find('.filename').text(file.name);
			
		})
		.on("submit", "form[name='form-manage-import']", function (e) {
			$that = $(this);
			var btn_control = $(this).find('.btn.choose_file__');
			btn_control.removeClass('btn-warning').addClass('btn-primary');
			e.preventDefault();
			$("#submit-import").attr('disabled', 'disabled');
			let file = document.getElementById("file").files[0];
			if (file === undefined) return Msg.error("Pastikan telah memilih file");
			$(".row-error .title").html('');
			$(".row-error .content").html('');
			bootbox.confirm({
				title: $lang.bootbox_title_confirmation,
				message: $lang.bootbox_message_confirm_upload+"<br>"+file.name,
				size: 'small',
				buttons: {
						confirm: {
								label: $lang.bootbox_btn_confirm,
								className: 'btn-success'
						},
						cancel: {
								label: $lang.bootbox_btn_no,
								className: 'btn-danger'
						}
				},
				callback: function (result) {
					if (result) {
						var spanLoader = '<span class="spanloader text-primary" style="position: absolute; top: 45%;left: 45%;z-index: 1000;font-size: 14px"><i class="fa fa-spin fa-5x text-primary fa-spinner"></i><br>Processing.... </span>';
						$("#modal-manage-import .modal-body").append($(spanLoader));
						var formData = new FormData($that[0]);
						$.ajax({
							url: base_url('farmasi/obat/import_'),
							type: 'POST',
							data: formData,
							async: false,
							cache: false,
							contentType: false,
							enctype: 'multipart/form-data',
							processData: false,
							success: function (response) {
								Msg.success(response.message);
								$tableData.ajax.reload();
								$($modalManageImport).modal('hide');
								
								$("#submit-import").removeAttr('disabled');
								if (response.duplicate && response.duplicate.length>0) {
									$(".row-error .content").html('');
									$(".row-error .title").html('sudah terdaftar (Duplicate):');
									var htm = '<ul class="striped-list ml-0 pl-3" > ';
									$.each(response.duplicate, function (i,item) {
										htm+='<li>'+item+'</li>'
									})
									htm += '</ >';
									$('.row-error .content').html(htm);
								} else {
									if (response.link) {
										location.href = response.link;
									}
								}
								
							},
							error: function (err) {
								var json = err.responseJSON;
								Msg.error(json.error);
								if (json.data && json.data.length>0) {
									$(".row-error .content").html('');
									$(".row-error .title").html('Error Data sheet tidak sesuai:');
									var htm = '<ul class="striped-list ml-0 pl-3" > ';
									$.each(json.data, function (i,item) {
										htm+='<li>'+item+'</li>'
									})
									htm += '</ >';
									$('.row-error .content').html(htm);
								}
	
							},
							complete: function (response) {
								btn_control.removeClass('btn-primary').addClass('btn-warning');
								$("#file").val('');
								$(".filename").text('');
								// loading_hide();
								$(".spanloader").remove();
							}
						})
					} else {
						alert("closed");
						btn_control.removeClass('btn-primary').addClass('btn-warning');
						$("#file").val('');
						$(".filename").text('');
					}
				}
			});
		})
	$("#satuanbeli").select2({
		minimumResultsForSearch: -1,
		placeholder: "Pilih Satuan",
		tags: true,
		ajax: { 
		url: base_url('farmasi/obat/select2_satuan_beli'),
		type: "post",
		dataType: 'json',
		delay: 250,
		data: function (params) {
			return {
				key: params.term
			};
		},
		processResults: function (search) {
			return {
				results: search
			};
		},
			cache: true
	}
	});
		
	$("#satuanobat").select2({
		minimumResultsForSearch: -1,
		placeholder: "Satuan",
		tags: true,
		ajax: { 
		url: base_url('farmasi/obat/select2_satuan_obat'),
		type: "post",
		dataType: 'json',
		delay: 250,
		data: function (params) {
			return {
				key: params.term
			};
		},
		processResults: function (search) {
			return {
				results: search
			};
		},
			cache: true
		}
	});
		
	

	$("#kategori").select2({
		minimumResultsForSearch: -1,
		placeholder: "Pilih Kategori Obat",
		tags: true,
		ajax: { 
		url: base_url('farmasi/obat/select2_kategori'),
		type: "post",
		dataType: 'json',
		delay: 250,
		data: function (params) {
			return {
				key: params.term
			};
		},
		processResults: function (search) {
			return {
				results: search
			};
		},
			cache: true
		}	
	})
	
	$("#supplier").select2({
		minimumResultsForSearch: -1,
		placeholder: "Pilih Supplier",
		tags: true,
		ajax: { 
		url: base_url('farmasi/obat/select2_supplier'),
		type: "post",
		dataType: 'json',
		delay: 250,
		data: function (params) {
			return {
				key: params.term
			};
		},
		processResults: function (search) {
			return {
				results: search
			};
		},
			cache: true
	}	
})


$('select#satuanobat').on('change', function() {
	var singleValues = $( "#satuanobat" ).find(":selected").text();
	$( "#label" ).html( singleValues );
  });
$('select#satuanbeli').on('change', function() {
	var singleValues = $( "#satuanbeli" ).find(":selected").text();
	$( "#label2" ).html( singleValues );
  });

$('#autocode').on("click",function(){
	http_request('farmasi/obat/searchcode','GET',{id: $(this).data('id')})
	.done(function(result){
		var data=result;
			$( "#kode" ).val(data);
	})
});

	$('[name="expired"]').datepicker({
		changeMonth: true,
		changeYear: true,
		showOtherMonths: true,
		selectOtherMonths: true,
		dateFormat: 'yy-mm-dd',
		reverseYearRange: true,
		yearRange: 'c:c+80',
		container: '#modal-manage-obat'
	})

const hitungJual = (hargaBeli, laba, isi) => {
	hargaBeli = parseFloat(hargaBeli);
	laba  = parseFloat(laba);
		
	hargaBeliisi = hargaBeli / isi;
	hasilLaba = (laba/100)*hargaBeliisi;
	hasilJual = hargaBeliisi + hasilLaba; 
	return (hasilJual); // jual price
}

const hitungLaba = (hargaBeli, hargaJual, isi) => {
	hargaBeli = parseFloat(hargaBeli);
	hargaJual = parseFloat(hargaJual);
	hargaBeliisi = hargaBeli /isi;
	hasil = ((hargaJual - hargaBeliisi )/hargaBeliisi)*100;
	return (hasil); // laba percentage
}
const hitungKonversi = ( stok, isi) => {
	hasil= stok*isi;
	return (hasil);
}

// Our use case
const	$beli = $('input[name="hargaBeli"]'),
		$laba = $('input[name="laba"]'), 
		$isi = $('input[name="isiperSatuanbeli"]'), 
		$jual = $('input[name="hargaJual"]'),
		$konversi = $('input[name="konversi"]'),
		$stok = $('input[name="stok"]'); 
	
$beli.add( $laba).add($isi).on('input', () => { // beli dan laba inputs events
	let jual = $beli.val();              // Default to harga beli
	if (( $laba.val().length || $isi.val().length )) {          // if value is entered- calculate harga jual
	jual = hitungJual($beli.val(), $laba.val(),$isi.val());
	}else{
		$laba.val(0);
		$isi.val(1);
		jual = hitungJual($beli.val(), $laba.val(),$isi.val());
	
	}
	$jual.val( Math.round(jual) );
});
$jual.on('input', () => {      // jual input events
	let laba = 0;                // Default to 0%
	if ( $jual.val().length ) {  // if value is entered- calculate the laba
	laba = hitungLaba($beli.val(), $jual.val(),$isi.val());
	}
	$laba.val( Math.round(laba) );
});

$stok.add($isi).on('input', () => {
	if ( $stok.val().length ) {          // if value is entered- calculate konversi
	konversi = hitungKonversi($stok.val(), $isi.val());
	}
	$konversi.val( konversi );
});

	
// $('#barcode_download').on('click',function(){
//     window.print();
//     return false;
// })

$(function(){
	$("#tambah").on("click",function(){
		var $img = $("#printBarcode").clone();
		$("#printBarcode").append($img);
	});
	$('#barcode_download').on('click', function() {
		$("#printBarcode").print({
			globalStyles : true,
			iframe : false
		})
	})
})