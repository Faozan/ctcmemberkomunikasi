class RestApi {
  constructor() {
    this.styles = () => {
      return {
        headerDefault: {
          fill: {
            fgColor: {
              rgb: 'FFFFFFFF'
            }
          },
          font: {
            color: {
              rgb: 'FF000000'
            },
            sz: 12,
            bold: true,
            underline: false
          },
          border: {
            top: {style: 'thin'},
            bottom: {style: 'thin'},
            left: {style: 'thin'},
            right: {style: 'thin'},
          }
        },
        cellStyle: {
          border: {
            top: {style: 'thin'},
            bottom: {style: 'thin'},
            left: {style: 'thin'},
            right: {style: 'thin'},
          }
        },
        headerDark: {
          fill: {
            fgColor: {
              rgb: 'FF000000'
            }
          },
          font: {
            color: {
              rgb: 'FFFFFFFF'
            },
            sz: 14,
            bold: true,
            underline: true
          }
        },
        cellPink: {
          fill: {
            fgColor: {
              rgb: 'FFFFCCFF'
            }
          }
        },
        cellGreen: {
          fill: {
            fgColor: {
              rgb: 'FF00FF00'
            }
          }
        }
      }
    }
  }
}
var Service = new RestApi();
module.exports = Service;