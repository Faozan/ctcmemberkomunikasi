const mysql = require('mariadb');
var Promise = require('promise');
const is_production = (process.env.NODE_ENV == 'PRODUCTION') ? true : false;
console.log("is production ", is_production)
const _env=process.env
const pool = mysql.createPool({
  connectionLimit: 100,
  host: (!is_production) ? _env.DB_HOST_DEV : _env.DB_HOST,
  user: (!is_production) ? _env.DB_USER_DEV : _env.DB_USER,
  password: (!is_production) ? _env.DB_PASS_DEV : _env.DB_PASS,
  database: (!is_production) ? _env.DB_NAME_DEV: _env.DB_NAME,
  timezone: 'Asia/Jakarta',
  skipSetTimezone: true,
  multipleStatements: true,
  waitForConnection: true,
  queueLimit: 0,
  permitSetMultiParamEntries: true  
})
module.exports={
    getConnection: ()=>{
      return new Promise((resolve,reject)=>{
        pool.getConnection().then(function(connection){
          resolve(connection);
        }).catch(function(error){
          reject(error);
        });
      });
    }
  } 