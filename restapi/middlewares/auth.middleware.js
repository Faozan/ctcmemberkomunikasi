var mdb_helper = require('../helpers/mdb.helper')
var jwt = require('jsonwebtoken');
var md5 = require('md5')
var privateKey = 'MMapp21^ctc'
var Promise=require('promise')
module.exports = {
    verifyAppKey: (req, res, next) => {
        const appkey = req.headers['c-appkey'];
        console.log(req.headers);
        if (appkey== undefined) return res.status(401).send('Your application is unregistered');
        mdb_helper.checkAppKey(req.headers['c-appkey'].trim()).then(result => {
            req.appkey = result.appkey;
            req.appname = result.appname;
            next();
        })
        .catch(err => {
            res.status(401).send(err);
        })
    },
    generateToken: (data) => {
        return new Promise((resolve, reject) => {
            try {
                resolve(jwt.sign(JSON.stringify(data), privateKey))
            } catch (error) {
                reject(error)
            }
        })
    },
    isAuthenticate: (req, res, next) => {
        var token = req.headers['c-access-token'] || req.body.token || req.query.token || false;
        if (token) {
            jwt.verify(token, privateKey, (err, decoded) => {
                if (err) {
                    return res.status(403).json({ message: 'Error : Invalid token. Please sign out and sign in back' });
                } else {
                    console.log(decoded)
                    req.decoded = decoded;
                    req.name = decoded.name
                    req.mem_id = decoded.mem_id.toString()
                    req.email=decoded.email
                    next();
                }
            });
        } else {    
            return res.status(401).send({message: 'Error: You are not logged in.'})
        }
    },
    isAuthenticateAdmin: (req, res, next) => {
        var token = req.headers['c-access-token'] || false;
        if (token) {
            jwt.verify(token, privateKey, (err, decoded) => {
                if (err) {
                    return res.status(403).json({ message: 'Invalid token. Please sign out and sign in back' });
                } else {
                    if (!decoded.userGroup || decoded.userGroup != 'admin') return res.status(403).json({ message: 'Invalid token. You are not signed in as admin' });
                    req.actions_code = decoded.actions_code.split(',')
                    req.decoded = decoded;
                    req.uid = decoded.uid
                    req.name = decoded.name
                    req.email = decoded.email
                    mdb_helper.execute(`SELECT actions_code,enabled,level FROM c_users WHERE uid=? LIMIT 1`, [req.uid])
                    .then(result => {
                        var dta = result[0]
                        if (dta.enabled == 0) return res.status(403).send({ message: 'Your account was disabled' })
                        req.actions_code = dta.actions_code.split(',')
                        if (dta.level == 'c-spadmin') {
                            req.actions_code.push(dta.level)
                            req.isSuperAdmin=true
                        }
                        console.log(decoded)
                        next();
                    }).catch(error => {
                        return res.status(401).send({message: error})
                    })
                    
                }
            });
        } else {    
            return res.status(401).send({message: 'Error: You are not logged in.'})
        }
    },
    isAllowAction: (access_list,accessID) => {
        if (access_list != null && (access_list.indexOf(accessID.toString()) > -1 || access_list.indexOf('c-spadmin')>-1)) {
            return true;
        } else {
            return false;
        }
    },
    extractToken: (token, cb) => {
        try {
            jwt.verify(token, privateKey, (err, decoded) => {
                cb(err, decoded)
            })
        } catch (error) {
            cb(error)
        }

    },
};