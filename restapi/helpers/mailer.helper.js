var nodemailer = require('nodemailer')
var smtpTransport = require('nodemailer-smtp-transport');
var Promise = require('promise');
let transporter = nodemailer.createTransport(smtpTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  auth: {
    user: process.env.ACCOUNT_GMAIL,
    pass: process.env.GMAIL_PASS
  }
}));
class Restapi{
  constructor() {
    this.sendEmail = (data) => new Promise((resolve, reject) => {
      const mailOptions = {
        from: { name: 'Mega Mall Batam Center', address: process.env.ACCOUNT_GMAIL},
        to: data.to,
        subject: data.subject,
        html: data.message
      };
      if(data.attachments) mailOptions.attachments=data.attachments
      transporter.sendMail(mailOptions, (err, info) => {
        if (err)
          return reject(err);
        resolve(info);
      });
    })
  }
}
var Service = new Restapi();
module.exports = Service