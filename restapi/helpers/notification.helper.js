var __io = _io; // getting from global variable io
var Promise = require('promise');
var _ = require("lodash")
var mdb=require('./mdb.helper');
var mw = require('../middlewares/auth.middleware')
var moment = require('moment-timezone')
moment.tz.setDefault("Asia/Jakarta")
const nsp = __io.of('/notification');
var FCMNode = require('fcm-node');
var serverKey = require('../configs/firebase-private-key.json')
var fcm = new FCMNode(serverKey)
var users = [];
_io.pushNotifFCM = (data) => {
  console.log('trigger notification')
  var message = {
      to: '/topics/'+data.topic,
      notification: {
        title: data.title,
        body: data.message || 'test body',
        sound: 'default',
        badge: '1'
      },
      data: data.data
  };
  console.log(message)
    fcm.send(message, (err, resp) => {
      if (err) console.log("err ",err)
      console.log(resp)
    })
}
var getUser = keys => {
    if (typeof keys == 'undefined') keys = {};
    return _.find(users, keys)
}
var addUser = data => {
  users.push(data)
  console.log(data)
  updateOnlineStatus(data, 'online')
}
var updateOnlineStatus = (data, status) => {
  return new Promise((resolve, reject) => {
    var is_online = (status == 'online') ? 1 : 0;
    var table = (data.is_admin) ? 'c_users' : 'c_members'
    var where=(data.is_admin) ? {uid: data.uid} : {mem_id: data.mem_id}
    var squery = `UPDATE ${table} SET is_online=?,last_online=? WHERE ?`;
    mdb.execute(squery, [is_online, moment().format("YYYY-MM-DD HH:mm:ss"), where])
    resolve("done")
  })
    
}
var deleteUser = id => {
    _.reject(users, { id: id })
}
var __socket=nsp
nsp.on('connection', function(socket) {
  var token = socket.handshake.query.token;
  _io._push_info = socket
 
    mw.extractToken(token, (err, decoded) => {
        if (err) console.log(err)
      var decoded = decoded;
        var name = decoded.name
        var mem_id = decoded.mem_id
      var email = decoded.email
      var is_admin=(decoded.userGroup && decoded.userGroup=='admin') ? true : false
        addUser({ id: socket.id,uid: decoded.uid, mem_id: mem_id, name: name, time: moment().format("YYYY-MM-DD HH:mm"),is_admin: is_admin });
        console.log(name + ' is online : at ' + socket.id)

    })

    socket.on("disconnect", () => {
        var user = _.find(users, { id: socket.id })
        if (typeof user != 'undefined') {
          console.log(user.name, ' is offline')
        }
        deleteUser(socket.id)
    })
    socket.on("info", data => {
        var recipt_id = data.recipt_id;
        var user = getUser({ id: socket.id })
        if (data.message == "") return false;
        var reply_of = "";
        if (data.reply_of && !_.isEmpty(data.reply_of)) {
            reply_of = `${data.reply_of.id}|${data.reply_of.user},${data.reply_of.time}:${data.reply_of.message}`;
        }
        if (recipt_id == 'admin') {
            getDefaultAdmin().then(recipts => {
                saveMessage({ sender: user.m_id, recipts: recipts, message: data.message, is_admin: true, user: user, reply_of: reply_of })
            }).catch(err => {
                console.log(err)
            })

        } else {
            saveMessage({ sender: user.m_id, recipts: recipt_id, message: data.message, user: user, reply_of: reply_of })
        }
    })
    socket.on("read-message", usr_msg_id => {
        var user = getUser({ id: socket.id })
        if (typeof usr_msg_id != 'undefined') {
            var split_dta = usr_msg_id.split('-');
            var msg_id = split_dta[1];
            // get group chat
            MYSQL.CHATBOX(`update t_chats a INNER JOIN t_chats b ON a.msg_to=b.msg_to AND LEFT(a.timestamp,16)=LEFT(b.timestamp,16) set a.is_read=1,b.is_read=1 WHERE a.msg_to=? AND a.msg_id=? AND (a.is_read=0 OR b.is_read=0)`, [user.m_id, msg_id], (err, result) => {
                if (err) console.log(err)

            })
        }
    })
})
module.exports = (app) => {
  app.get('/test-send-notif/', (req, res, nextError) => {
    var body = req.query;
    _io._push_info.emit('info',body.message)
      res.send("OK")

  })
  app.get('/test-firebase', (req, res, nextError) => {
    var body=req.query
    var topic=body.topic || 'promo-and-winner'
    var message = {
      to: '/topics/'+topic,
      notification: {
        title: body.title || 'test title',
        body: body.message || 'test body',
        sound: body.sound || 'default',
        badge: '1'
      },
      data: body.data || {},
     // collapse_key: topic,
    };

    fcm.send(message, (err, resp) => {
      if (err) return nextError(err)
      console.log(err)
      console.log(resp)
      res.send({message: 'success',resp})
    })
  })
  app.post('/test-firebase', (req, res, nextError) => {
    var body=req.query
    var topic=body.topic || 'promo-and-winner'
    var message = {
      to: '/topics/'+topic,
      notification: {
        title: body.title || 'test title',
        body: body.message || 'test body',
        sound: body.sound || 'default',
        badge: '1'
      },
      data: body.data || {},
     // collapse_key: topic,
    };

    fcm.send(message, (err, resp) => {
      if (err) return nextError(err)
      console.log(err)
      console.log(resp)
      res.send({message: 'success',resp})
    })
  })
}