class RestApi {
    constructor() {
      this.base_restapi = (url) => {
        var uri=(url.indexOf('/')==0) ? url : `/${url}`
        return `${global.domain_restapi}${uri}`
      }
      this.download = (url) => {
        var uri=(url.indexOf('/')==0) ? url : `/${url}`
        return `${global.domain_restapi}/downloads${uri}`
      }
      this.images = url => {
        var uri=(url.indexOf('/')==0) ? url : `/${url}`
        return `${global.domain_restapi}/images${uri}`
      }
    }
}
var Service = new RestApi();
module.exports = Service;