
var Promise = require('bluebird')
var moment = require('moment-timezone')
moment.tz.setDefault("Asia/Jakarta")
var _ = require('lodash')
var mdb=require('./mdb.helper')
const fs = require('fs')
var path=require('path')
const spawn = require('child_process').spawn
const CronJob=require('cron').CronJob
const zlib = require('zlib')
const findRemoveSync = require('find-remove')
var mailer=require('./mailer.helper')
class Backup{
  constructor() {
    this.doBackup = () => {
      return new Promise((resolve, reject) => {
        const gzip = zlib.createGzip();
        var filename=`bupsysdb/${process.env.DB_NAME}-backup-${moment().format("YYYYMMDD HHmm")}.sql.gz`
        var wstream = fs.createWriteStream(`files/${filename}`)
        var mysqldump = spawn('mysqldump', [
          '-u',
          'root',
          `-p${process.env.DB_ROOTPASS}`,
          '--compact',
          process.env.DB_NAME
        ]);
      
        mysqldump.stdout.on('error', reject).pipe(gzip).pipe(wstream)
        wstream.on('finish', () => {
          mdb.execute(`INSERT INTO c_backup_list (filename) VALUES (?)`, [filename])
          var days = parseInt(process.env.KEEP_BACKUP_IN_DAYS) || 7
          var send_email_to = process.env.SEND_BACKUP_TO_EMAIL || ''
          if (send_email_to!='') mailer.sendEmail({
            to: send_email_to,
            subject: 'Backup DB - Membership App Mega Mall ',
            message: 'Backup file is attached',
            attachments: [{path: path.join(__dirname,`../files/${filename}`)}]
          })
          var result = findRemoveSync(path.join(__dirname, '../files/bupsysdb'), { age: { seconds: days*24*3600 }, extensions: '.gz' })
          var keys = _.keys(result)
          var map_file = _.map(keys, f => {
            return f.replace(path.join(__dirname,'../files/'),'')
          })
          if (map_file.length > 0) mdb.execute(`DELETE FROM c_backup_list WHERE filename IN (?)`, [map_file])
          resolve('backup finish')
        })
        wstream.on('error', reject)
      })
    }
    this.doBackupToMnt = () => {
      return new Promise((resolve, reject) => {
        const gzip = zlib.createGzip();
        var filename=`${process.env.DB_NAME}-backup-${moment().format("YYYYMMDD HHmm")}.sql.gz`
        var wstream = fs.createWriteStream(`/mnt/backupDB/${filename}`)
        var mysqldump = spawn('mysqldump', [
          '-u',
          'root',
          `-p${process.env.DB_ROOTPASS}`,
          '--compact',
          process.env.DB_NAME
        ]);
      
        mysqldump.stdout.on('error', reject).pipe(gzip).pipe(wstream)
        wstream.on('finish', () => {
          var result = findRemoveSync('/mnt/backupDB/', { age: { seconds: 3*24*3600 }, extensions: '.gz' })
          resolve('backup finish')
        })
        wstream.on('error', reject)
      })
    }
    this.cleanupTemp = () => {
      return new Promise((resolve, reject) => {
        findRemoveSync(path.join(__dirname, '../files/temp/'), { age: { seconds: 1 * 24 * 3600 }, files: '*.*' })
        resolve("done")
      })
    }
  }
}
var Service = new Backup();

new CronJob({
  cronTime: '0 50 23 * * *',
  onTick: ()=>{
    Service.doBackup()
  },
  onComplete: () => {
    //console.log("task complete")
  },
  start: true,
  timeZone: 'Asia/Jakarta'
})
new CronJob({
  cronTime: '0 59 23 * * *',
  onTick: ()=>{
    Service.doBackupToMnt()
  },
  onComplete: () => {
    //console.log("task complete")
  },
  start: true,
  timeZone: 'Asia/Jakarta'
})
// auto cleanup temp folder
new CronJob({
  cronTime: '0 10 20 * * *',
  onTick: ()=>{
    Service.cleanupTemp()
  },
  onComplete: () => {
    //console.log("task complete")
  },
  start: true,
  timeZone: 'Asia/Jakarta'
})

module.exports = Service