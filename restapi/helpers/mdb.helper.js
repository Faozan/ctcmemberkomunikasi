
var Promise = require('bluebird')
var moment = require('moment-timezone')
moment.tz.setDefault("Asia/Jakarta")
var _ = require('lodash')
const db = require('../configs/mdb.conf')
class Mdb{
  constructor() {
    this.execute = async (_query, _params, other_params, callback = null) => {
      var with_calc = false
      if (typeof other_params != 'function' && typeof other_params != 'undefined' && other_params.calc_rows) {
        with_calc = true; _query = _query.replace(/SELECT/, 'SELECT SQL_CALC_FOUND_ROWS ');
      }
      if (typeof other_params == 'function' || (callback!=null && callback=='function')) {
        try {
          const conn = await mm_db.getConnection()
          const result = await conn.query(_query, _params)
          if (!with_calc) {
            conn.release()
            if(with_calc) return callback(null,result)
            return other_params(null,result)
          }
          const result2 = await conn.query(`SELECT FOUND_ROWS() as count`, {})
          conn.release()
          var result3={
            select: result,
            total: result2[0].count
          }
          if (with_calc) return callback(null, result3)
          return other_params(null,result3)
        } catch (err) {
          if (with_calc) return callback(err)
          return other_params(err)
        }
      } else {
        return new Promise(async (resolve, reject) => {
          try {
            const conn = await mm_db.getConnection()
            const result = await conn.query(_query, _params)
            if (!with_calc) {
              conn.release(); return resolve(result)
            }
            const result2 = await conn.query(`SELECT FOUND_ROWS() as count`, {})
            conn.release()
            resolve({
              select: result,
              total: result2[0].count
            })
          } catch (err) {
            reject(err)
          }
        })
      }
    }
    this.executeBatch = async (_query, _params) => {
      return new Promise(async (resolve, reject) => {
        try {
          const conn = await mm_db.getConnection()
          var result = await conn.batch(_query, _params)
          conn.release()
          resolve(result)
        } catch (err) {
          reject(err)
        }
      })
    }
    this.checkAppKey = appId => {
      return new Promise(async (resolve, reject) => {
        try {
          const result = await this.execute(`SELECT keyid,appname FROM c_app_keys WHERE keyid=?`, [appId])
          if (result.length > 0) return resolve({ appkey: result[0].keyid, appname: result[0].appname })
          reject("Error occured with unauthorized access")
        } catch (err) {
          console.log(err)
          reject(err)
        }
      })
    }
    this.validateEmail = email => {
      return new Promise((resolve, reject) => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(email).toLowerCase())) return resolve(email.toLowerCase())
        return reject("Invalid email")
      })
    }
    this.validateNumber = (name,numb) => {
      return new Promise((resolve, reject) => {
        const re = /^[0-9]+$/
        if (re.test(numb.trim())) return resolve(numb.trim())
        reject(`Invalid ${name}`)
      })
    }
    this.checkRequired = (body, required_fields) => {
      return new Promise(async (resolve, reject) => {
        try {
          var map = _.compact(_.map(required_fields, field => {
            if (!body[field] || body[field] == "") return field;
            return false;
          }))
          if (map.length > 0) return reject(`Required: ${map.join(", ")}`)
          resolve("OK")
        } catch (error) {
          reject(error)
        }
      })
    }
    this.getMemberId = () => {
      return new Promise(async (resolve, reject) => {
        try {
          // var ym = moment().format("YYMM");
          var ym = moment().format("YY-");
          const last_member = await this.execute(`SELECT MAX(mem_id) as mem_id FROM ${tableMembers} WHERE mem_id LIKE ? ORDER BY mid DESC LIMIT 1`, [ym + '%'])
          var id = ym + '0000001';
          if (last_member.length > 0 && last_member[0].mem_id!=null) {
            var mem_id = last_member[0].mem_id;
            id = `${parseInt(mem_id.split('').splice(-6).join("")) + 1}`
            var idlen=id.toString().length
            if (idlen < 7) id = '0'.repeat(7 - idlen) + '' + id
            id=ym+''+id
          }
          return resolve(id);  
        } catch (error) {
          reject(error)
        }
      })
    }
    this.buildSearchParams = (sSearch,skips=[]) => {
      return new Promise((resolve, reject) => {
        if (!sSearch || sSearch == null) resolve({sWhere: "",sParams: []})
        var sWhere = "";
        var sParams = [];
        var sSkipsVal = {}
        _.each(sSearch, (val, key) => {
          if (skips.indexOf(key) == -1) {
            var key2 = (key.indexOf('::') > -1) ? key.split('::') : key;
            key2 = (typeof key2 == 'object') ? key2[1] : key2;
            var key = key.replace('::', '.');
            if (val != "") {
              sWhere += (sWhere == "") ? " WHERE " : " AND ";
              sWhere += key + " LIKE ?";
              val = (val.indexOf('!!') > -1) ? val.trim().replace('!!', '') : '%' + val.trim() + '%';
              sParams.push(val);
            }
          } else {
            if(val!='')
              sSkipsVal[key]=val
          }
        })
        resolve({sWhere: sWhere, sParams: sParams,sSkip: sSkipsVal})
      })
    }
    this.voucherNumber = (voucher) => {
      var length = 7;
      var vcr = voucher
      var len = vcr.toString().length
      if(len<length) vcr='0'.repeat(length-len)+''+vcr
      return vcr
    }
  }
}
var Service = new Mdb();
module.exports = Service