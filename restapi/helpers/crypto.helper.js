var crypto = require('crypto');
var md5=require('md5')
var Promise = require('promise');
class Restapi{
  constructor() {
    this.alogrithm = 'aes-256-ctr'
    this.secretKey = md5('rTu59am1n92021Ap1')
    this.iv = crypto.randomBytes(16)
    this.encrypt = async (text) => {
      return new Promise(async resolve => {
        const cipher = crypto.createCipheriv(this.alogrithm, this.secretKey, this.iv)
        const encrypted = Buffer.concat([cipher.update(text), cipher.final()])
        return resolve({ iv: this.iv.toString('hex'), content:encrypted.toString('hex') })
      })
    }
    this.encryptJson = async json => {
      return new Promise(async resolve => {
        const e = await this.encrypt(JSON.stringify(json))
        resolve(JSON.stringify(e))
      })
    }
    this.decrypt = async hash => {
      return new Promise(async resolve => {
        const decipher = crypto.createDecipheriv(this.alogrithm, this.secretKey, Buffer.from(hash.iv, 'hex'))
        const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
        return resolve(decrpyted.toString());
      })
    }
    this.decryptJson = async object => {
      return new Promise(async resolve => {
        const e = await this.decrypt(JSON.parse(object))
        resolve(JSON.parse(e))
      })
    }
  }
}
var Service = new Restapi();
module.exports = Service