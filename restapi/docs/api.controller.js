var moment = require('moment-timezone');
moment().tz("Asia/Jakarta").format();
var _ = require('lodash');
var fg = require('fast-glob')
var fs = require("fs-extra");
module.exports={
  configure: (app) => {
    app.set("view engine", "pug")
    app.get('/doc-api/:anyId?', (req, res, nextError) => {
      app.set('views', __dirname)
      const main = async () => {
        var files = await fg.sync('./docs/*.json', {})
        var map=await Promise.all(files.map(async file=>{
          const docs = await fs.readJSON(file)
          return docs
        }));
        if(req.params.anyId && req.params.anyId=='json') return res.send(map)
        res.render('api_view', { base_url: '../doc-api/', data: map });
      }
      main().catch(nextError)
    })
  }
}