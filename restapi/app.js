var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var cookieParser = require('cookie-parser');
var app = express();
app.use(cors());
//app.use(express.static(path.join(__dirname, 'publics')));
app.use(logger('dev'));
app.disable('x-powered-by')
var http = require('http');
var server = http.Server(app);

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(cookieParser());
app.use((req, res, next) => {
    res.removeHeader('Server');
    return next();
});
// var io = require('socket.io')(server);
// global._io = io;

app.set('trust proxy', true);
require('dotenv').config({path: __dirname + '/.env'})
require('./docs/api.controller').configure(app)

app.use((req, res, next) => {
    var host = req.ip || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    host = (host.indexOf('0.0.1') > -1 || host.indexOf('::1') > -1) ? host = 'http://localhost/ctc-memberkomunitas/restapi' : host;
    req.client_host = host;
    return next();
})
// catch 404 and forward to error handler
app.use((req, res, next) => {
    var err = new Error(req + res);
    err.status = 404;
    next(err);
});
// error handler
app.use((err, req, res, next) => {
    // res.locals.message = err.message;
    // res.locals.error = req.app.get('env') === 'development' ? err : {};
    if (err.status == 404) {
        res.status(err.status).send("Error 404! Not Found");
    } else {
        if (typeof err == 'string') err = new Error(err)
        res.status(400).send({ status: 400, message: err.message });
    }
});
global.domain_restapi = false
var port = process.env.PORT;
server.listen(port, () => {
    var _host = server.address().address;
    if (_host == '::') _host = 'http://localhost'
    if(process.env.NODE_ENV=='PRODUCTION') _host='http://68.183.186.159'
    var _port = server.address().port;
    console.log(_host)
    if (!global.domain_restapi) global.domain_restapi = `${_host}:${_port}`
    console.log('memberkomunitas api is running and listening on port ' + _port);
});
module.exports = app;